FROM node:14

# Install base dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        jq \
        python3-pip \
        python3-setuptools \
        gpg-agent \
        time \
    && pip3 install --upgrade pip \
    && apt-get -y autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install awscli
RUN pip3 install awsebcli
